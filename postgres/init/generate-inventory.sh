#!/bin/bash
set -e

echo "SELECT 'CREATE DATABASE inventory' WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'inventory')\gexec" | psql

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_NAME" <<-EOSQL
    CREATE USER inventory PASSWORD 'inventory';
    GRANT ALL PRIVILEGES ON DATABASE inventory TO inventory;
EOSQL
