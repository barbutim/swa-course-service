package cz.cvut.fel.swa.school.domain;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Entity
@Data
public class Parallel {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private Long teacher;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "students")
    private List<Long> students;

    @NotNull
    private ParallelTypeEnum type;

    @Column(columnDefinition = "DATE")
    @NotNull
    private LocalDate fromDate;

    @Column(columnDefinition = "DATE")
    @NotNull
    private LocalDate toDate;

    @ManyToOne
    @NotNull
    private Course course;
}
