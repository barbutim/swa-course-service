package cz.cvut.fel.swa.school.domain;

public enum ParallelTypeEnum {
    LECTURE,
    EXERCISE,
    LABORATORY
}
