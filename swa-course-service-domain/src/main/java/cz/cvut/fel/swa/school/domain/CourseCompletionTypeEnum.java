package cz.cvut.fel.swa.school.domain;

public enum CourseCompletionTypeEnum {
    AE, // Assessment and examination
    A, // Assessment
    GA // Graded assessment
}
