package cz.cvut.fel.swa.school.repository;

import cz.cvut.fel.swa.school.domain.Course;
import org.springframework.data.repository.CrudRepository;

public interface CourseRepository extends CrudRepository<Course, Long> {

}
