package cz.cvut.fel.swa.school.repository;

import cz.cvut.fel.swa.school.domain.Course;
import cz.cvut.fel.swa.school.domain.Parallel;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ParallelRepository extends CrudRepository<Parallel, Long> {
    List<Parallel> findAllByCourse(Course course);
}
