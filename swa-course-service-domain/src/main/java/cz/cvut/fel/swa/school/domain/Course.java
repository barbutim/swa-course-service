package cz.cvut.fel.swa.school.domain;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Entity
@Data
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @NotNull
    private String code;

    @NotNull
    private String name;

    @NotNull
    private CourseCompletionTypeEnum completion;

    @NotNull
    private Integer credits;

    /*@OneToMany(mappedBy = "course", cascade = CascadeType.ALL)
    @JsonIgnore
    private Parallel parallels;*/
}
