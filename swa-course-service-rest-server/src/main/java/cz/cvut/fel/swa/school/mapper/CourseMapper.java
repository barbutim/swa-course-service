package cz.cvut.fel.swa.school.mapper;

import cz.cvut.fel.swa.school.domain.Course;
import cz.cvut.fel.swa.school.generated.model.dto.CourseDto;
import org.mapstruct.Mapper;

@Mapper
public interface CourseMapper {
    CourseDto toDto(Course course);
}
