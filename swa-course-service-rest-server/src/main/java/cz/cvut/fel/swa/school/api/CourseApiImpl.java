package cz.cvut.fel.swa.school.api;

import cz.cvut.fel.swa.school.generated.controller.CourseApiDelegate;
import cz.cvut.fel.swa.school.generated.controller.ParallelApiDelegate;
import cz.cvut.fel.swa.school.generated.model.dto.*;
import cz.cvut.fel.swa.school.mapper.CourseMapper;
import cz.cvut.fel.swa.school.mapper.ParallelMapper;
import cz.cvut.fel.swa.school.service.CourseService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.List;
import java.util.Optional;

@RestController
@Log4j2
@RequiredArgsConstructor
public class CourseApiImpl implements CourseApiDelegate, ParallelApiDelegate {

    private final CourseService courseService;
    private final CourseMapper courseMapper;
    private final ParallelMapper parallelMapper;

    @Override
    public Optional<NativeWebRequest> getRequest() {
        return CourseApiDelegate.super.getRequest();
    }

    @Override
    public ResponseEntity<CourseDto> createCourse(CreateCourseRequestDto courseDto) {
        return ResponseEntity.ok(courseMapper.toDto(courseService.createCourse(courseDto)));
    }

    @Override
    public ResponseEntity<CourseDto> updateCourse(UpdateCourseRequestDto courseDto) {
        return ResponseEntity.ok(courseMapper.toDto(courseService.updateCourse(courseDto)));
    }

    @Override
    public ResponseEntity<Void> deleteCourse(String courseId) {
        courseService.deleteCourse(courseId);
        return ResponseEntity.ok(null);
    }

    @Override
    public ResponseEntity<List<CourseDto>> getCourses() {
        return ResponseEntity.ok(courseService.getCourses().stream().map(courseMapper::toDto).toList());
    }

    @Override
    public ResponseEntity<CourseDto> getCourse(String courseId) {
        return ResponseEntity.ok(courseMapper.toDto(courseService.getCourse(courseId)));
    }

    @Override
    public ResponseEntity<ParallelDto> createParallel(CreateParallelRequestDto parallelDto) {
        return ResponseEntity.ok(parallelMapper.toDto(courseService.createParallel(parallelDto)));
    }

    @Override
    public ResponseEntity<ParallelDto> updateParallel(UpdateParallelRequestDto parallelDto) {
        return ResponseEntity.ok(parallelMapper.toDto(courseService.updateParallel(parallelDto)));
    }

    @Override
    public ResponseEntity<Void> deleteParallel(String parallelId) {
        courseService.deleteParallel(parallelId);
        return ResponseEntity.ok(null);
    }

    @Override
    public ResponseEntity<List<ParallelDto>> getParallels() {
        return ResponseEntity.ok(courseService.getParallels().stream().map(parallelMapper::toDto).toList());
    }

    @Override
    public ResponseEntity<ParallelDto> getParallel(String parallelId) {
        return ResponseEntity.ok(parallelMapper.toDto(courseService.getParallel(parallelId)));
    }

    @Override
    public ResponseEntity<List<ParallelDto>> getParallelsByCourseId(String courseId) {
        return ResponseEntity.ok(courseService.getParallelsByCourseId(courseId).stream().map(parallelMapper::toDto).toList());
    }

    @Override
    public ResponseEntity<Void> addStudentToParallel(String parallelId, String studentId, AddStudentToParallelDto addStudentToParallelDto) {
        courseService.addStudentToParallel(studentId, parallelId);
        return ResponseEntity.ok(null);
    }

    @Override
    public ResponseEntity<Void> addTeacherToParallel(String parallelId, String teacherId, AddTeacherToParallelDto addTeacherToParallelDto) {
        courseService.addTeacherToParallel(teacherId, parallelId);
        return ResponseEntity.ok(null);
    }
}
