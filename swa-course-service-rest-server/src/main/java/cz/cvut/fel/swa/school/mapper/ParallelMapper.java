package cz.cvut.fel.swa.school.mapper;

import cz.cvut.fel.swa.school.domain.Parallel;
import cz.cvut.fel.swa.school.generated.model.dto.ParallelDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface ParallelMapper {
    @Mapping(source = "course.id", target = "courseId")
    ParallelDto toDto(Parallel parallel);
}
