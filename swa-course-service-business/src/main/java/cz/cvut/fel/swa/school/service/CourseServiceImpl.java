package cz.cvut.fel.swa.school.service;

import com.google.common.collect.Lists;
import cz.cvut.fel.swa.school.domain.Course;
import cz.cvut.fel.swa.school.domain.CourseCompletionTypeEnum;
import cz.cvut.fel.swa.school.domain.Parallel;
import cz.cvut.fel.swa.school.domain.ParallelTypeEnum;
import cz.cvut.fel.swa.school.generated.model.dto.CreateCourseRequestDto;
import cz.cvut.fel.swa.school.generated.model.dto.CreateParallelRequestDto;
import cz.cvut.fel.swa.school.generated.model.dto.UpdateCourseRequestDto;
import cz.cvut.fel.swa.school.generated.model.dto.UpdateParallelRequestDto;
import cz.cvut.fel.swa.school.repository.CourseRepository;
import cz.cvut.fel.swa.school.repository.ParallelRepository;
import jakarta.ws.rs.NotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Log4j2
@RequiredArgsConstructor
public class CourseServiceImpl implements CourseService {

    private final CourseRepository courseRepository;
    private final ParallelRepository parallelRepository;

    @Override
    public Course createCourse(CreateCourseRequestDto courseDto) {
        Course course = new Course();
        course.setCode(courseDto.getCode());
        course.setName(courseDto.getName());
        course.setCompletion(CourseCompletionTypeEnum.valueOf(courseDto.getCompletion().name()));
        course.setCredits(courseDto.getCredits());
        return courseRepository.save(course);
    }

    @Override
    public Course updateCourse(UpdateCourseRequestDto courseDto) {
        Course course = courseRepository.findById(Long.valueOf(courseDto.getId()))
                .orElseThrow(() -> new NotFoundException("Could not find Course with ID " + courseDto.getId()));
        if(courseDto.getCode() != null) {
            course.setCode(courseDto.getCode());
        }
        if(courseDto.getName() != null) {
            course.setName(courseDto.getName());
        }
        if(courseDto.getCompletion() != null) {
            course.setCompletion(CourseCompletionTypeEnum.valueOf(courseDto.getCompletion().name()));
        }
        if(courseDto.getCredits() != null) {
            course.setCredits(courseDto.getCredits());
        }
        return courseRepository.save(course);
    }

    @Override
    public void deleteCourse(String courseId) {
        courseRepository.deleteById(Long.valueOf(courseId));
    }

    @Override
    public List<Course> getCourses() {
        return Lists.newArrayList(courseRepository.findAll());
    }

    @Override
    public Course getCourse(String courseId) {
        return courseRepository.findById(Long.valueOf(courseId))
                .orElseThrow(() -> new NotFoundException("Could not find Course with ID " + courseId));
    }

    @Override
    public Parallel createParallel(CreateParallelRequestDto parallelDto) {
        Parallel parallel = new Parallel();
        parallel.setTeacher(Long.valueOf(parallelDto.getTeacher()));
        parallel.setType(ParallelTypeEnum.valueOf(parallelDto.getType().name()));
        if(parallelDto.getFromDate().isAfter(parallelDto.getToDate())) {
            throw new IllegalArgumentException("Parallel start date cannot be set after the end date");
        }
        parallel.setFromDate(parallelDto.getFromDate());
        parallel.setToDate(parallelDto.getToDate());
        Course course = courseRepository.findById(Long.valueOf(parallelDto.getCourseId()))
                .orElseThrow(() -> new NotFoundException("Could not find Course with ID " + parallelDto.getCourseId()));
        parallel.setCourse(course);
        return parallelRepository.save(parallel);
    }

    @Override
    public Parallel updateParallel(UpdateParallelRequestDto parallelDto) {
        Parallel parallel = parallelRepository.findById(Long.valueOf(parallelDto.getId()))
                .orElseThrow(() -> new NotFoundException("Could not find Parallel with ID " + parallelDto.getId()));
        if(parallelDto.getTeacher() != null) {
            parallel.setTeacher(Long.valueOf(parallelDto.getTeacher()));
        }
        if(parallelDto.getType() != null) {
            parallel.setType(ParallelTypeEnum.valueOf(parallelDto.getType().name()));
        }
        if(parallelDto.getFromDate().isAfter(parallelDto.getToDate())) {
            throw new IllegalArgumentException("Parallel start date cannot be set after the end date");
        }
        if(parallelDto.getFromDate() != null) {
            parallel.setFromDate(parallelDto.getFromDate());
        }
        if(parallelDto.getToDate() != null) {
            parallel.setToDate(parallelDto.getToDate());
        }
        if(parallelDto.getCourseId() != null) {
            Course course = courseRepository.findById(Long.valueOf(parallelDto.getCourseId()))
                    .orElseThrow(() -> new NotFoundException("Could not find Course with ID " + parallelDto.getCourseId()));
            parallel.setCourse(course);
        }
        return parallelRepository.save(parallel);
    }

    @Override
    public void deleteParallel(String parallelId) {
        parallelRepository.deleteById(Long.valueOf(parallelId));
    }

    @Override
    public List<Parallel> getParallels() {
        return Lists.newArrayList(parallelRepository.findAll());
    }

    @Override
    public Parallel getParallel(String parallelId) {
        return parallelRepository.findById(Long.valueOf(parallelId))
                .orElseThrow(() -> new NotFoundException("Could not find Parallel with ID " + parallelId));
    }

    @Override
    public List<Parallel> getParallelsByCourseId(String courseId) {
        Course course = courseRepository.findById(Long.valueOf(courseId))
                .orElseThrow(() -> new NotFoundException("Could not find Course with ID " + courseId));
        return Lists.newArrayList(parallelRepository.findAllByCourse(course));
    }

    @Override
    public void addTeacherToParallel(String teacherId, String parallelId) {
        Parallel parallel = getParallel(parallelId);
        if (parallel.getTeacher() == null) {
            throw new NotFoundException("This parallel already has teacher");
        }
        parallel.setTeacher(Long.valueOf(teacherId));
        parallelRepository.save(parallel);
    }

    @Override
    public void addStudentToParallel(String studentId, String parallelId) {
        Parallel parallel = getParallel(parallelId);
        parallel.getStudents().add(Long.valueOf(studentId));
        parallelRepository.save(parallel);
    }
}
