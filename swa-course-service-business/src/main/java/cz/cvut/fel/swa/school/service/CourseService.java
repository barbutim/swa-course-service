package cz.cvut.fel.swa.school.service;

import cz.cvut.fel.swa.school.domain.Course;
import cz.cvut.fel.swa.school.domain.Parallel;
import cz.cvut.fel.swa.school.generated.model.dto.CreateCourseRequestDto;
import cz.cvut.fel.swa.school.generated.model.dto.CreateParallelRequestDto;
import cz.cvut.fel.swa.school.generated.model.dto.UpdateCourseRequestDto;
import cz.cvut.fel.swa.school.generated.model.dto.UpdateParallelRequestDto;

import java.util.List;

public interface CourseService {
    Course createCourse(CreateCourseRequestDto courseDto);

    Course updateCourse(UpdateCourseRequestDto courseDto);

    void deleteCourse(String courseId);

    List<Course> getCourses();

    Course getCourse(String courseId);

    Parallel createParallel(CreateParallelRequestDto parallelDto);

    Parallel updateParallel(UpdateParallelRequestDto parallelDto);

    void deleteParallel(String parallelId);

    List<Parallel> getParallels();

    Parallel getParallel(String parallelId);

    List<Parallel> getParallelsByCourseId(String courseId);

    void addTeacherToParallel(String teacherId, String parallelId);

    void addStudentToParallel(String studentId, String parallelId);
}
