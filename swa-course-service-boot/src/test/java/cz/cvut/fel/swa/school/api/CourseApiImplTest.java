package cz.cvut.fel.swa.school.api;

import cz.cvut.fel.swa.school.CourseServiceApplication;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;

@SpringBootTest(webEnvironment = DEFINED_PORT)
@ContextConfiguration(classes = CourseServiceApplication.class)
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@Testcontainers
class CourseApiImplTest {

    /*@Container
    private static final PostgreSQLContainer<?> postgreSQLContainer = new PostgreSQLContainer<>("postgres:latest")
            .withDatabaseName("integration-tests-db").withUsername("username").withPassword("password");

    static {
        postgreSQLContainer.start();
    }

    @Autowired
    private CourseService courseService;

    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry dynamicPropertyRegistry) {
        System.setProperty("spring.datasource.driver-class-name", postgreSQLContainer.getDriverClassName());
        dynamicPropertyRegistry.add("spring.datasource.url", postgreSQLContainer::getJdbcUrl);
        dynamicPropertyRegistry.add("spring.datasource.username", postgreSQLContainer::getUsername);
        dynamicPropertyRegistry.add("spring.datasource.password", postgreSQLContainer::getPassword);
    }

    @BeforeAll
    public static void setUp() {
        RestAssured.port = DEFAULT_PORT;
    }

    @Test
    void createCourse() {
        CreateCourseRequestDto courseDto = new CreateCourseRequestDto("B4M36SWA", "Softwarové architektury", CourseCompletionTypeEnumDto.AE, 6);

        var course = RestAssured.given()
            .contentType(ContentType.JSON)
            .body(Json.pretty(courseDto))
            .put("/course")
            .then()
            .assertThat()
            .statusCode(200)
            .extract()
            .as(CourseDto.class);

        Assertions.assertEquals(courseDto.getCode(), course.getCode());
        Assertions.assertNotNull(course.getId());
        Assertions.assertEquals(courseDto.getName(), course.getName());
        Assertions.assertEquals(courseDto.getCompletion(), course.getCompletion());
        Assertions.assertEquals(courseDto.getCredits(), course.getCredits());
    }

    @Test
    void createParallelForCourse() {
        var courseDto = courseService.createCourse(new CreateCourseRequestDto("B4M36SWA", "Softwarové architektury", CourseCompletionTypeEnumDto.AE, 6));
        var parallelDto = courseService.createParallel(new CreateParallelRequestDto("Miroslav Bureš", ParallelTypeEnumDto.EXERCISE, LocalDate.now(), LocalDate.now().plusDays(15), courseDto.getId().toString()));

        var course = RestAssured.given()
                .contentType(ContentType.JSON)
                .body(Json.pretty(courseDto))
                .put("/course")
                .then()
                .assertThat()
                .statusCode(200)
                .extract()
                .as(CourseDto.class);

        var parallel = RestAssured.given()
                .contentType(ContentType.JSON)
                .body(Json.pretty(parallelDto))
                .put("/parallel")
                .then()
                .assertThat()
                .statusCode(200)
                .extract()
                .as(ParallelDto.class);

        Assertions.assertEquals(courseDto.getCode(), course.getCode());
        Assertions.assertNotNull(course.getId());
        Assertions.assertEquals(courseDto.getName(), course.getName());
        Assertions.assertEquals(courseDto.getCredits(), course.getCredits());

        Assertions.assertEquals(parallelDto.getTeacher(), parallel.getTeacher());
        Assertions.assertNotNull(parallel.getId());
        Assertions.assertEquals(parallelDto.getFromDate(), parallel.getFromDate());
        Assertions.assertEquals(parallelDto.getToDate(), parallel.getToDate());
        Assertions.assertEquals(parallelDto.getCourse().getId(), Long.valueOf(parallel.getCourseId()));
    }*/
}