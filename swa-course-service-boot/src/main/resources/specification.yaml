openapi: 3.0.0

info:
  description: Open API specification for Course service
  version: "1.0.0"
  title: Course Service Specification
tags:
  - name: Course
    description: Course calls

paths:
  /course:
    put:
      tags:
        - Course
      summary: create course
      operationId: createCourse
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CreateCourseRequest'
      responses:
        '200':
          description: Returns course
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Course'
        '400':
          $ref: '#/components/responses/BadRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
        '404':
          $ref: '#/components/responses/NotFound'
        '500':
          $ref: '#/components/responses/InternalServerError'
    patch:
      tags:
        - Course
      summary: Update course
      operationId: updateCourse
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/UpdateCourseRequest'
      responses:
        '200':
          description: Returns course
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Course'
        '400':
          $ref: '#/components/responses/BadRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
        '404':
          $ref: '#/components/responses/NotFound'
        '500':
          $ref: '#/components/responses/InternalServerError'
    get:
      tags:
        - Course
      summary: Get all courses
      operationId: getCourses
      description: returns all courses
      responses:
        '200':
          description: Returns courses
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Course'
        '400':
          $ref: '#/components/responses/BadRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
        '404':
          $ref: '#/components/responses/NotFound'
        '500':
          $ref: '#/components/responses/InternalServerError'
  /course/{courseId}:
    get:
      tags:
        - Course
      summary: Get course by id
      operationId: getCourse
      parameters:
        - in: path
          required: true
          name: courseId
          description: Course id
          schema:
            type: string
      responses:
        '200':
          description: Returns course
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Course'
        '400':
          $ref: '#/components/responses/BadRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
        '404':
          $ref: '#/components/responses/NotFound'
        '500':
          $ref: '#/components/responses/InternalServerError'
    delete:
      tags:
        - Course
      operationId: deleteCourse
      parameters:
        - in: path
          required: true
          name: courseId
          description: Course id
          schema:
            type: string
      responses:
        '200':
          description: ok
        '400':
          $ref: '#/components/responses/BadRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
        '404':
          $ref: '#/components/responses/NotFound'
        '500':
          $ref: '#/components/responses/InternalServerError'
  /parallel:
    put:
      tags:
        - Parallel
      summary: create parallel for course id
      operationId: createParallel
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CreateParallelRequest'
      responses:
        '200':
          description: Returns parallel
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Parallel'
        '400':
          $ref: '#/components/responses/BadRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
        '404':
          $ref: '#/components/responses/NotFound'
        '500':
          $ref: '#/components/responses/InternalServerError'
    patch:
      tags:
        - Parallel
      summary: Update parallel
      operationId: updateParallel
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/UpdateParallelRequest'
      responses:
        '200':
          description: Returns parallel
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Parallel'
        '400':
          $ref: '#/components/responses/BadRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
        '404':
          $ref: '#/components/responses/NotFound'
        '500':
          $ref: '#/components/responses/InternalServerError'
    get:
      tags:
        - Parallel
      summary: Get all parallels
      operationId: getParallels
      description: returns all parallels
      responses:
        '200':
          description: Returns parallels
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Parallel'
        '400':
          $ref: '#/components/responses/BadRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
        '404':
          $ref: '#/components/responses/NotFound'
        '500':
          $ref: '#/components/responses/InternalServerError'
  /parallel/{parallelId}:
    get:
      tags:
        - Parallel
      summary: Get parallel by parallel id
      operationId: getParallel
      parameters:
        - in: path
          required: true
          name: parallelId
          description: Parallel id
          schema:
            type: string
      responses:
        '200':
          description: Returns parallel
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Parallel'
        '400':
          $ref: '#/components/responses/BadRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
        '404':
          $ref: '#/components/responses/NotFound'
        '500':
          $ref: '#/components/responses/InternalServerError'
    delete:
      tags:
        - Parallel
      operationId: deleteParallel
      parameters:
        - in: path
          required: true
          name: parallelId
          description: Parallel id
          schema:
            type: string
      responses:
        '200':
          description: ok
        '400':
          $ref: '#/components/responses/BadRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
        '404':
          $ref: '#/components/responses/NotFound'
        '500':
          $ref: '#/components/responses/InternalServerError'
  /parallel/{courseId}/course:
    get:
      tags:
        - Parallel
      summary: Get parallel by course id
      operationId: getParallelsByCourseId
      parameters:
        - in: path
          required: true
          name: courseId
          description: Course id
          schema:
            type: string
      responses:
        '200':
          description: Returns parallels
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Parallel'
        '400':
          $ref: '#/components/responses/BadRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
        '404':
          $ref: '#/components/responses/NotFound'
        '500':
          $ref: '#/components/responses/InternalServerError'
  /parallel/{parallelId}/add-teacher/{teacherId}:
    put:
      tags:
        - Parallel
      summary: Add teacher to parallel
      operationId: addTeacherToParallel
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/AddTeacherToParallel'
      parameters:
        - in: path
          required: true
          name: parallelId
          description: Parallel id
          schema:
            type: string
        - in: path
          required: true
          name: teacherId
          description: Teacher id
          schema:
            type: string
      responses:
        '400':
          $ref: '#/components/responses/BadRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
        '404':
          $ref: '#/components/responses/NotFound'
        '500':
          $ref: '#/components/responses/InternalServerError'
  /parallel/{parallelId}/add-student/{studentId}:
    put:
      tags:
        - Parallel
      summary: Add student to parallel
      operationId: addStudentToParallel
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/AddStudentToParallel'
      parameters:
        - in: path
          required: true
          name: parallelId
          description: Parallel id
          schema:
            type: string
        - in: path
          required: true
          name: studentId
          description: Student id
          schema:
            type: string
      responses:
        '400':
          $ref: '#/components/responses/BadRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
        '404':
          $ref: '#/components/responses/NotFound'
        '500':
          $ref: '#/components/responses/InternalServerError'
components:
  responses:
    BadRequest:
      description: Indicates a nonspecific client error
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Exception'
    NotFound:
      description: The specified resource was not found
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Exception'
    Unauthorized:
      description: Unauthorized request - sent when the client either provided invalid credentials
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Exception'
    InternalServerError:
      description: Internal server error
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Exception'

  schemas:
    Exception:
      type: object
      properties:
        code:
          type: string
          example: 400
        title:
          type: string
          example: Wrong input parameter
        message:
          type: string
          example: Wrong parameter for endpoint /example/endpoint, wrong endpointId value.

    CreateCourseRequest:
      type: object
      required:
        - code
        - name
        - completion
        - credits
      properties:
        code:
          type: string
        name:
          type: string
        completion:
          $ref: '#/components/schemas/CourseCompletionTypeEnum'
        credits:
          type: integer
    UpdateCourseRequest:
      type: object
      required:
        - id
      properties:
        id:
          type: string
        code:
          type: string
        name:
          type: string
        completion:
          $ref: '#/components/schemas/CourseCompletionTypeEnum'
        credits:
          type: integer
    Course:
      type: object
      required:
        - id
        - code
        - name
        - completion
        - credits
      properties:
        id:
          type: string
        code:
          type: string
        name:
          type: string
        completion:
          $ref: '#/components/schemas/CourseCompletionTypeEnum'
        credits:
          type: integer
    AddTeacherToParallel:
      type: object
      required:
        - teacherId
        - parallelId
      properties:
        teacherId:
          type: string
        parallelId:
          type: string
    AddStudentToParallel:
      type: object
      required:
        - studentId
        - parallelId
      properties:
        studentId:
          type: string
        parallelId:
          type: string
    CreateParallelRequest:
      type: object
      required:
        - teacher
        - type
        - fromDate
        - toDate
        - courseId
      properties:
        teacher:
          type: string
        type:
          $ref: '#/components/schemas/ParallelTypeEnum'
        fromDate:
          type: string
          format: date
        toDate:
          type: string
          format: date
        courseId:
          type: string
    UpdateParallelRequest:
      type: object
      required:
        - id
      properties:
        id:
          type: string
        teacher:
          type: string
        type:
          $ref: '#/components/schemas/ParallelTypeEnum'
        fromDate:
          type: string
          format: date
        toDate:
          type: string
          format: date
        courseId:
          type: string
    Parallel:
      type: object
      required:
        - id
        - teacher
        - type
        - fromDate
        - toDate
        - courseId
        - students
      properties:
        id:
          type: string
        teacher:
          type: string
        type:
          $ref: '#/components/schemas/ParallelTypeEnum'
        fromDate:
          type: string
          format: date
        toDate:
          type: string
          format: date
        courseId:
          type: string
        students:
          type: array
          items:
            type: string
    CourseCompletionTypeEnum:
      type: string
      enum:
        - AE
        - A
        - GA
    ParallelTypeEnum:
      type: string
      enum:
        - LECTURE
        - EXERCISE
        - LABORATORY
